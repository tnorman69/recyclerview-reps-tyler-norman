package com.example.randomcolorbutton.view.adapter

import android.graphics.Color
import android.graphics.ColorSpace
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.core.graphics.convertTo
import androidx.core.graphics.toColor
import androidx.core.graphics.toColorLong
import androidx.recyclerview.widget.RecyclerView
import com.example.randomcolorbutton.R
import com.example.randomcolorbutton.databinding.FragmentGenerateColorBinding
import com.example.randomcolorbutton.databinding.ListColorsBinding

class ColorAdapter : RecyclerView.Adapter<ColorAdapter.LovelyViewholder>() {

    private lateinit var data: List<Int>

    class LovelyViewholder(
        private val binding: ListColorsBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun apply(item: Int) {
            binding.listColors.setCardBackgroundColor(item)
            var hexColor: String? = java.lang.String.format("#%06X", 0xFFFFFF and item)
            binding.listColorsText.text = "$hexColor"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LovelyViewholder {
        val binding = ListColorsBinding.inflate(LayoutInflater.from(parent.context))
        return LovelyViewholder(binding)
    }

    override fun onBindViewHolder(holder: LovelyViewholder, position: Int) {
        val item = data[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun giveData(data: List<Int>){
        this.data = data
    }
}