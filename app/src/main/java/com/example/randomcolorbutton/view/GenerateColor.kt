package com.example.randomcolorbutton.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.randomcolorbutton.Resource
import com.example.randomcolorbutton.databinding.FragmentGenerateColorBinding
import com.example.randomcolorbutton.view.adapter.ColorAdapter
import com.example.randomcolorbutton.viewmodel.MainViewModel

class GenerateColor : Fragment() {

    private var _binding: FragmentGenerateColorBinding? = null
    private val binding: FragmentGenerateColorBinding get() = _binding!!

    private val viewmodel by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGenerateColorBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initListeners()
    }

    private fun initListeners() {
        binding.button5.setOnClickListener {
            viewmodel.getColors()
        }
    }

    private fun initObservers() = with(viewmodel) {
        color.observe(viewLifecycleOwner){ viewState ->
            when(viewState){
                Resource.Loading -> {
                    // Circular progress spinning still
                }
                is Resource.Success -> {
                    binding.recycleViewFun.layoutManager = LinearLayoutManager(requireContext())
                    binding.recycleViewFun.adapter = ColorAdapter().apply {
                        giveData(viewState.data)
                    }
                }
            }
        }
    }
}
