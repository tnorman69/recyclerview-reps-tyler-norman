package com.example.randomcolorbutton.model

interface Api {
    suspend fun generateColor() : List<Int>
}