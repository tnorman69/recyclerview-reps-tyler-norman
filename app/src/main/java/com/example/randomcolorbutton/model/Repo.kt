package com.example.randomcolorbutton.model

import com.example.randomcolorbutton.Resource
import com.example.randomcolorbutton.randomColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object Repo {
    val apiService = object : Api {
        override suspend fun generateColor(): List<Int> {
            val mycolors: MutableList<Int> = ArrayList()
            for (i in 0..4){
                mycolors.add(i, randomColor)
            }
                return mycolors
        }
    }
        suspend fun getdata() = withContext(Dispatchers.IO){
            return@withContext Resource.Success(data = apiService.generateColor())
        }



}