package com.example.randomcolorbutton

import android.graphics.Color
import kotlin.random.Random

val randomColor
    get() = Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))